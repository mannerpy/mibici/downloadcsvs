import requests 
from bs4 import BeautifulSoup 
import re
import html

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'}
response = requests.get("https://mibici.net/es/datos-abiertos/", headers=headers)

soup = BeautifulSoup(response.text, features="html.parser")
a_elements = soup.find_all("a")
a_elements = filter(lambda x: x['href'].endswith(".csv"), a_elements)
a_elements = filter(lambda x: x['href'].__contains__("datos_abiertos"), a_elements)
a_elements = list(a_elements)
a_elements = [x for x in a_elements if x['href'] is not None]

print("Se encontraron {} elementos".format(len(a_elements)))

import requests
import os 

BASE_URL = "https://www.mibici.net"

if not os.path.exists("csv_files"):
    os.mkdir("csv_files")

for x in a_elements:
    filename = x['href'].split("/")
    filename = "csv_files/{}".format(filename[-1])

    url = BASE_URL + x['href']
    if os.path.exists(filename):
        print("Archivo descargado previamente: {}".format(filename))
        continue # En caso de tener descargado previamente el archivo

    response = requests.get(url,  headers=headers).text
    
    with open(filename, "w") as file:
        file.write(response)
        file.close()
    
    print(f"DESCARGADO: {filename}")
